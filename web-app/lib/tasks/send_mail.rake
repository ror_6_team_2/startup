task send_mail: :environment do
  mails = SubscribedMail.all
  mails.each do |mail|
    project = Project.find_by_id(mail.project_id)
    UserMailer.send_report_mail(project, mail).deliver_now if project.status == "Активный"
  end
end
task check_expiration_date: :environment do
	mails = SubscribedMail.all
	  mails.each do |mail|
	    project = Project.find_by_id(mail.project_id)
	    UserMailer.send_deadline_mail(project, mail).deliver_now if project.deadline.change(:hour => 0) == DateTime.now.change(:hour => 0) + 3.days
  end
end