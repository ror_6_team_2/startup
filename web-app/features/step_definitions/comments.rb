### Сценарий Создания
Допустим(/^залогинен пользователь с email "([^"]*)" и паролем "([^"]*)"$/) do |email, password|
  visit('/users/sign_in')
  within('#new_user') do 
    fill_in('Электронная почта', with: email)
    fill_in('Пароль', with: password)
    click_button('Log in')
  end
end
Если('пользователь находится на странице проекта Zimperiun') do
  visit('/projects/1')
end

Если("он создает комментарий с данными:") do |table|
  within('#new_comment') do
    fill_in('comment_body', with: table.hashes[0][:body])
  end
  click_button('Создать Комментарий')
end

То("комментарий с текстом {string} видно в списке комментариев") do |string|
  page.has_xpath?('//div/p', :text => string)
end

@tag
### Сценарий Удаления
Если("видит свой комментарий c сообщением {string}") do |string|
  page.has_xpath?('//div/p', :text => string)
end

Когда("он нажимает на значек удалить") do
  accept_confirm do
    find(:xpath, "//a[@href='/projects/1/comments/1' and @data-method='delete']").click
  end
end
Тогда("видит страницу проекта без своего сообщения {string}") do |string|
   page.has_no_content?(string)
end

### Сценарий редактирования

Когда("он нажимает на значек редактирования") do
  find(:xpath, "//a[contains(@href,'/projects/1/comments/1/edit')]").click
end

Тогда("видит модальное окно редактирования с пометкой {string}") do |string|
   page.has_content?(string)  
end

Если("вводит новый текст {string}") do |string|
  within('form#edit_comment_1') do
    fill_in('comment_body', with: string)
  end
end

Если("нажимает кнопку {string}") do |string|
   click_button(string)
end

Тогда("видит страницу проекта с отредактированные сообщением c текстом {string}") do |string|
  page.has_xpath?('//div/p', :text => string)
end

Тогда("пометкой {string}") do |string|
  page.has_xpath?('//div/p', :text => string)
end
