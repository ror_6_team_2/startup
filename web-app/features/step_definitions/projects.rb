
### Создание нового проекта

Допустим(/^залогинен пользователь с email "([^"]*)" и password "([^"]*)"$/) do |email, password|
  visit('/users/sign_in')
  within('#new_user') do 
    fill_in('Электронная почта', with: email)
    fill_in('Пароль', with: password)
    click_button('Log in')
  end
end

Если('пользователь находится на странице создания проекта') do
  visit('/projects/new')
end

Если("он создает проект с данными:") do |table|
  within('#new_project') do
    fill_in('Название проекта', with: table.hashes[0][:name])
    fill_in('Бюджет', with: table.hashes[0][:budget])
    fill_in('Собранный бюджет', with: table.hashes[0][:budget_donated])
    fill_in('Счет проекта', with: table.hashes[0][:account])
    select(table.hashes[0][:status], from: 'Статус')
    select(table.hashes[0][:stage], from: 'Стадия проекта')
    attach_file('project_image', File.join(Dir.pwd, 'features', 'step_definitions', 'test_files', 'project.jpg'))
    page.execute_script('$(tinymce.editors[0].setContent("test description"))')
    page.execute_script('$(tinymce.editors[1].setContent("test budget description"))')
    page.execute_script("$('#project_deadline_1i').val('2018')")
    page.execute_script("$('#project_deadline_2i').val('10')")
    page.execute_script("$('#project_deadline_3i').val('10')")
    page.execute_script("$('#project_deadline_4i').val('23')")
    page.execute_script("$('#project_deadline_5i').val('12')")
    select(table.hashes[0][:category], from: "Категория проекта")

  end
  click_button('Создать Проект')
end

То(/^проект с именем "([^"]*)" видно в списке всех проектов$/) do |name|
  page.has_xpath?('//a[contains(text(), "#{name}")]')
end

### Редактирование проекта

Если('пользователь находится на странице проекта Zimperiun в панели администратора') do
  visit('/projects/1/edit')
end

Если("он вводит в поле name текст") do |table|
  within('#edit_project_1') do
    fill_in('Название проекта', with: table.hashes[0][:name])
  end
  click_button('Сохранить Проект')
end

Тогда("видит страницу проекта с новым именем {string}") do |string|
  page.has_xpath?('//div/h1', :text => string)
end


### Удаление проекта

Если('пользователь находится на странице всех проектов в панели администратора') do
  visit('/admin/projects')
end

Когда("он нажимает на кнопку удалить") do
  accept_confirm do
    find(:xpath, "//td[text()='Aerosmith']/..//a[contains(@class, 'delete_link')]").click
  end
end

Если('заходит на страницу всех проектов на панели администратора') do
  visit('/admin/projects')
end

Тогда("он не видит своего проекта с именем {string}") do |string|
   page.has_no_content?(string)
end


