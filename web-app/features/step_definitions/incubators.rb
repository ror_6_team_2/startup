
### Создание нового инкубатора

Допустим(/^залогинен пользователь с email "([^"]*)" и password "([^"]*)"$/) do |email, password|
  visit('/users/sign_in')
  within('#new_user') do 
    fill_in('Электронная почта', with: email)
    fill_in('Пароль', with: password)
    click_button('Log in')
  end
end

Если('пользователь находится на странице создания инкубатора') do
  visit('/incubators/new')
end

Если("он создает инкубатор с данными:") do |table|
  within('#new_incubator') do
    fill_in('Название инкубатора', with: table.hashes[0][:name])
    attach_file('incubator_image', File.join(Dir.pwd, 'features', 'step_definitions', 'test_files', 'project.jpg'))
  end
  click_button('Создать Инкубатор')
end

То(/^инкубатор с именем "([^"]*)" видно в списке всех инкубаторов$/) do |name|
  page.has_xpath?('//a[contains(text(), "#{name}")]')
end

### Редактирование инкубатора

Допустим('пользователь находится на странице инкубатора Энактус в панели администратора') do
  visit('/incubators/3/edit')
end

Если("он вводит в поле Название инкубатора новое имя") do |table|
  within('#edit_incubator_3') do
    fill_in('Название инкубатора', with: table.hashes[0][:name])
  end
  click_button('Сохранить Инкубатор')
end

Тогда("видит страницу инкубатора с новым именем {string}") do |string|
  page.has_xpath?('//div/h1', :text => string)
end


### Удаление инкубатора

Если('пользователь находится на странице всех инкубаторов в панели администратора') do
  visit('/admin/incubators')
end

Когда("он нажимает на кнопку удалить для инкубатора Энактус") do
  accept_confirm do
    find(:xpath, "//td[text()='Энактус']/..//a[contains(@class, 'delete_link')]").click
  end
end

Если('заходит на страницу всех инкубаторов на панели администратора') do
  visit('/admin/incubators')
end

Тогда("он не видит свой инкубатор с именем {string}") do |string|
   page.has_no_content?(string)
end


