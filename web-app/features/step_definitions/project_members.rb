### Сценарий Добавления
Если("он нажимает на ссылку {string}") do |string|
  click_link(string)
end

И("он добавляет участника с данными:") do |table|
  within('#new_project_member') do
    fill_in('project_member_name', with: table.hashes[0][:name])
    fill_in('project_member_role', with: table.hashes[0][:role])
  end
  click_button('Отправить')
end

То("участника с именем {string} видно в таблице участников") do |string|
  within('#member_table') do
    page.has_xpath?('//td', :text => string)
  end
end

### Сценарий Удаления
И("видит участника с именем {string}") do |string|
  within('#member_table') do
    page.has_xpath?('//td', :text => string)
  end
end

Когда("он нажимает на удаление") do
  accept_confirm do
    find(:xpath, "//a[@href='/projects/1/project_members/1' and @data-method='delete']").click
  end
end
Тогда("видит страницу проекта без участника с именем {string}") do |string|
   page.has_no_content?(string)
end

### Сценарий Редактирования

Когда("он нажимает на имя {string}") do |string|
  find(:xpath, "//a[contains(@href,'/projects/1/project_members/1/edit')]").click
end

Если("вводит новое имя {string}") do |string|
  within('form#edit_project_member_1') do
    fill_in('project_member_name', with: string)
  end
end

Тогда("видит страницу проекта с отредактированным именем {string}") do |string|
  page.has_xpath?('//td', :text => string)
end
