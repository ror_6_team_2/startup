Допустим("пользователь на странице всех проектов") do 
  visit('/projects') 
end 
 
Если("он выбирает из списка сортировки Дата создания по убыванию") do 
  find("option[value='created_at_desc']").click 
end 
 
Тогда("он нажимает кнопку {string}") do |string| 
  click_button(string) 
end 
 
То("первым проектом в списке будет проект с названием {string}") do |string| 
  find(:xpath,"//div[@class='row crowdfunding_projects']/div[1]/div[1]/div[1]/h3/a[text()='#{string}']") 
end 
 
То("вторым будет {string}") do |string| 
  find(:xpath,"//div[@class='row crowdfunding_projects']/div[2]/div[1]/div[1]/h3/a[text()='#{string}']") 
end 

Если("он выбирает из списка сортировки Дата создания по возрастанию") do 
  find("option[value='created_at_asc']").click 
end 
 
Если("он выбирает из списка сортировки Сроку сбора по убыванию") do 
  find("option[value='deadline_desc']").click 
end 
 
Если("он выбирает из списка сортировки Сроку сбора по возрастанию") do 
  find("option[value='deadline_asc']").click 
end 
 
Если("он выбирает из списка сортировки По сбору денег по убыванию") do 
  find("option[value='donation_ratio_desc']").click 
end 
 
Если("он выбирает из списка сортировки По сбору денег по возрастанию") do 
  find("option[value='donation_ratio_asc']").click 
end