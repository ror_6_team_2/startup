class ProjectMembersController < ApplicationController
  before_action :set_project, only: [:new, :show, :create, :edit, :update, :destroy]

  def index

  end

  def new
    @member = @project.project_members.build
    authorize @member
    respond_to do |format|
      format.html
      format.js
    end
  end
  
  def edit
    @member = @project.project_members.find(params[:id])
    authorize @member
  end

  def create
    @member = @project.project_members.create(project_member_params)
    authorize @member
    respond_to do |format|
      format.html { redirect_to(@member.project) }
    end
  end

  def update
    @member = @project.project_members.find(params[:id])
    authorize @member
    @member.update(project_member_params)
    respond_to do |format|
      format.html { redirect_to(@member.project) }
    end
  end

  def destroy
    @member = @project.project_members.find(params[:id])
    authorize @member
    @member.destroy
    respond_to do |format|
      format.html { redirect_to(@member.project) }
    end
  end

  private

    def set_project
      @project = Project.find(params[:project_id])
    end

    def project_member_params
      params.require(:project_member).permit(:name, :role, :resume, :project_id, :user_id)
    end
end