class FeedbacksController < ApplicationController
	def new
	end

	def create
		email = User.find_by(role:"superadmin").email
	  UserMailer.send_feedback(email, params[:name], params[:user_email], params[:body]).deliver_now
	  redirect_back(fallback_location: root_path, notice: 'Mail was successfully sent.')
	end
end
