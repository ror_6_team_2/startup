class UserProjectsController < ApplicationController
  def index
  	authorize self
  	@all_projects = []
  	@subscribed_user = SubscribedMail.where(email: current_user.email)
  	@subscribed_user.each do |project|
  	  @all_projects << project.project
  	end
  end
end
