require 'builder'

class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy]
  layout 'index_layout', only: [:index]

  def index

    @filterrific = initialize_filterrific(Project, params[:filterrific])
    @filterrific.select_options = {
        sorted_by: Project.options_for_sorted_by
    }
    @projects = Project.filterrific_find(@filterrific).page(params[:page])

    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    change_project_status(@project)
    click_counter(@project)
    @subscribed_mail = SubscribedMail.new

  end

  # GET /projects/new
  def new
    @project = Project.new
    authorize @project
  end

  # GET /projects/1/edit
  def edit
    authorize @project
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.create(project_params)
    authorize @project
    assign_other_category_name(@project)
    @project.cancel_date = @project.deadline

    respond_to do |format|
      if @project.save
        format.html {redirect_to root_path, notice: 'Project was successfully created.'}
        format.json {render :show, status: :created, location: @project}
      else
        format.html {render :new}
        format.json {render json: @project.errors, status: :unprocessable_entity}
      end
    end

  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    authorize @project
    respond_to do |format|
      if @project.update(project_params)
        format.html {redirect_to @project, notice: 'Project was successfully updated.'}
        format.json {render :show, status: :ok, location: @project}
      else
        format.html {render :edit}
        format.json {render json: @project.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    authorize @project
    @project.destroy
    respond_to do |format|
      format.html {redirect_to projects_url, notice: 'Project was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_project
    @project = Project.find(params[:id])
  end

  def change_project_status(project)
    if project.deadline <= Time.now or project.cancel_date <=Time.now
      project.status = 'Неактивный'
    else
      project.status = 'Активный'
    end
  end

  def assign_other_category_name(project)
    if project.category_id == Category.find_by(name: 'Другое').id
      @category_other = Category.create(name: project.other_category)
      project.category_id = @category_other.id
    end
  end
  def click_counter(project)
    project.update(click: (project.click + 1))
  end
  # Never trust parameters from the scary internet, only allow the white list through.
  def project_params
    params.require(:project).permit(:name, :description, :budget, :budget_description, :budget_donated, :stage, :account, :status, :deadline, :cancel_date, :cancel_reason, :author_comment, :other_category, :category_id, :image, :file)
  end
end
