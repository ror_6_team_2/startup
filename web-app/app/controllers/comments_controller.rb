class CommentsController < ApplicationController
  before_action :authenticate_user!

  def new
    @comment = @commentable.comments.new(:ancestry => params[:ancestry])
    @ancestry = params[:ancestry]
    respond_to do |format|
      format.js
    end
  end
 
  def create
    @comment = @commentable.comments.new comment_params
    authorize @comment
    @comment.user = current_user
    @comment.save
    redirect_to project_path(@commentable)
  end

  def edit
    @comment = @commentable.comments.find(params[:id])
    authorize @comment
  end
  
  def update
    @comment = @commentable.comments.find(params[:id])
    authorize @comment
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_back(fallback_location: root_path) }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @comment = @commentable.comments.find(params[:id]) 
    authorize @comment
    @comment.destroy
    respond_to do |format|
      format.html { redirect_back(fallback_location: root_path) }
    end
  end

  private
  
    def comment_params
      params.require(:comment).permit(:body, :ancestry)
    end

  
end
