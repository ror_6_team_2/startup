class IncubatorsController < InheritedResources::Base

  private

    def incubator_params
      params.require(:incubator).permit(:name, :image)
    end
end

