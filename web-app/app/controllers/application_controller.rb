class ApplicationController < ActionController::Base
  
  include Pundit

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :store_user_location!, if: :storable_location?

  
  private

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :occupation, :work_experience,
                                                         :country, :region, :age])
      devise_parameter_sanitizer.permit(:account_update, keys: [:name, :occupation, :work_experience,
                                                         :country, :region, :age])
    end

    def storable_location?
      request.get? && is_navigational_format? && !devise_controller? && !request.xhr? 
    end

    def store_user_location!
      # :user is the scope we are authenticating
      store_location_for(:user, request.fullpath)
    end

    def access_denied(exception)
      flash[:danger] = exception.message
      redirect_to root_url
    end
    
    def user_not_authorized
      flash[:alert] = "You are not authorized to perform this action."
      redirect_to(root_url)
    end

  protected 
    def after_sign_up_path_for(resource_or_scope)
      stored_location_for(resource_or_scope) || super
    end
end
