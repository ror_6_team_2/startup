class SubscribedMailsController < ApplicationController

  def new
    @subscribed_mail = SubscribedMail.new
  end

  def create
    @subscribed_mail = SubscribedMail.new(subscribed_mail_params)
    @project = Project.find_by_id(@subscribed_mail.project_id)
    if @subscribed_mail.save
      UserMailer.send_mail(@project, @subscribed_mail).deliver_now
      redirect_back(fallback_location: root_path, notice: 'Mail was successfully sent.')
    else
      render 'new'
    end
  end

  def destroy
    @subscribed_mail = SubscribedMail.find(params[:id])
    @subscribed_mail.destroy
    redirect_to projects_url, notice: 'You were successfully unsubscribed.'
  end
  private

  def subscribed_mail_params
    params.require(:subscribed_mail).permit(:name, :email, :project_id)
  end
end
