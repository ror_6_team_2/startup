class MobilnikPaymentsController < ApplicationController
  require 'rest-client'
 
  def user_info_request 
    session[:project_id] = params[:project_id]
    @project_id = params[:project_id]
  end

  def admin_phone_number_request
    session[:project_id] = params[:project_id]
    @project_id = params[:project_id]
  end

  def mobilnik_code_request
    session[:account_number] = params[:account_number]
    session[:amount] = params[:amount]
    phone_number = params[:phone_number]
    session[:phone_number] = phone_number
    @project_id = session[:project_id]

    resource = RestClient::Resource.new("https://mobilnik.kg/register/?phonenumber=#{phone_number}&client=4", :timeout => 20, :open_timeout => 20)
    response = resource.get
    parsed_response = ActiveSupport::JSON.decode(response.to_str)
    respond_to do |format|
      if parsed_response['status_code'] == 0
        format.js {render 'user_code_request'}
      else
        format.js {render 'error_validation'}
      end
    end
  end

  def mobilnik_token_request
  	phone_number = session[:phone_number]
  	project_id = session[:project_id]
  	user_sms_code = params[:user_sms_code] 
    account_number = session[:account_number]
    amount = session[:amount]

    resource = RestClient::Resource.new("https://mobilnik.kg/authentication_by_otp/?phonenumber=#{phone_number}&client=4&code=#{user_sms_code}&phone_id=#{phone_number}", :timeout => 20, :open_timeout => 20)
    response = resource.get
    parsed_response = ActiveSupport::JSON.decode(response.to_str)
    respond_to do |format|
      if parsed_response['status_code'] == 0
      	token = parsed_response["token"]
        MobilniksWorker.perform_async(phone_number, token, account_number, amount, project_id)
        format.html {redirect_to project_path(project_id), notice: 'Спасибо, Ваш запрос обрабатывается, в течении пяти минут должна прийти смс об успешной транзакции' }
      else
        format.html {redirect_to project_path(project_id), notice: 'Ошибка, Попробуйте еще раз'}
      end
    end
  end

  def mobilnik_code_request_for_admin
    phone_number = params[:phone_number]
    session[:phone_number] = phone_number
    @project_id = session[:project_id]

    resource = RestClient::Resource.new("https://mobilnik.kg/register/?phonenumber=#{phone_number}&client=4", :timeout => 20, :open_timeout => 20)
    response = resource.get
    parsed_response = ActiveSupport::JSON.decode(response.to_str)
    respond_to do |format|
      if parsed_response['status_code'] == 0
        format.js {render 'admin_code_request'}
      else
        format.js {render 'error_validation'}
      end
    end
  end

  def mobilnik_token_request_for_admin
    phone_number = session[:phone_number]
    project_id = session[:project_id]
    user_sms_code = params[:user_sms_code] 

    resource = RestClient::Resource.new("https://mobilnik.kg/authentication_by_otp/?phonenumber=#{phone_number}&client=4&code=#{user_sms_code}&phone_id=#{phone_number}", :timeout => 20, :open_timeout => 20)
    response = resource.get
    parsed_response = ActiveSupport::JSON.decode(response.to_str)
    respond_to do |format|
      if parsed_response['status_code'] == 0
        token = parsed_response["token"]
        MobilniksReturnWorker.perform_async(phone_number, token, project_id)
        format.html {redirect_to project_path(project_id), notice: 'Средства будут возвращены' }
      else
        format.html {redirect_to project_path(project_id), notice: 'Ошибка, Попробуйте еще раз'}
      end
    end
  end

end