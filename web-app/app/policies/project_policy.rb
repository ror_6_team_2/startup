class ProjectPolicy < ApplicationPolicy

  attr_reader :user, :project

  def initialize(user, project)
    @user = user
    @project = project
  end

  def index?
    true
  end
  
  def new?
    user.present? && user.superadmin?
  end

  def create?
    user.present? && user.superadmin?
  end

  def edit?
    user.present? && user.superadmin?
  end
 
  def update?
    return true if user.present? && user.superadmin?
  end
 
  def destroy?
    return true if user.present? && user.superadmin?
  end
    
end