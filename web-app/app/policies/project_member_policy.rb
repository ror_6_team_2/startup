class ProjectMemberPolicy < ApplicationPolicy

  attr_reader :user, :project_member

  def initialize(user, project_member)
    @user = user
    @member = project_member
  end

  def index?
    true
  end
  
  def new?
    user.present? && user.superadmin?
  end

  def create?
    user.present? && user.superadmin?
  end

  def edit?
    user.present? && user.superadmin?
  end
 
  def update?
    return true if user.present? && user.superadmin?
  end
 
  def destroy?
    return true if user.present? && user.superadmin?
  end
    
end