class CategoryPolicy < ApplicationPolicy

  attr_reader :user, :category

  def initialize(user, category)
    @user = user
    @category = category
  end

  def index?
    user.present? && user.superadmin?
  end
  
  def new?
    user.present? && user.superadmin?
  end

  def show?
    user.present? && user.superadmin?
  end

  def create?
    user.present? && user.superadmin?
  end

  def edit?
    user.present? && user.superadmin?
  end
 
  def update?
    return true if user.present? && user.superadmin?
  end
 
  def destroy?
    return true if user.present? && user.superadmin?
  end
    
end