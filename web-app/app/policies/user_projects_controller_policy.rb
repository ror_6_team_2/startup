class UserProjectsControllerPolicy < ApplicationPolicy

  attr_reader :user, :user_project

  def initialize(user, user_project)
    @user = user
    @user_project = user_project
  end

  def index?
    user.present?
  end
    
end