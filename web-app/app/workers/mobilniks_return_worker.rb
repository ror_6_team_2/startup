class MobilniksReturnWorker
  include Sidekiq::Worker
  sidekiq_options :retry => 5

  def perform(phone_number, token, project_id)
    MobilnikPayment.where(project_id: project_id).find_each do |return_data|
      resource = RestClient::Resource.new "https://mobilnik.kg/transfer_request/?phonenumber=#{phone_number}&id=#{phone_number}&token=#{token}&client=4&service_id=1&customer_id=#{return_data.account_number}&amount=#{return_data.amount}&comment=vozvrat&status=2"
      response = resource.get
      parsed_response = ActiveSupport::JSON.decode(response.to_str)
      if parsed_response['status_code'] == 0
        begin
          Project.where(:id => project_id).update_all(["budget_donated = budget_donated - ?", return_data.amount])
          return_data.delete
        rescue Exception 
          return
        end 
      end
    end
  end
    
end