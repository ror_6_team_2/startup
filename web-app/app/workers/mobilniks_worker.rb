class MobilniksWorker
  include Sidekiq::Worker
  sidekiq_options :retry => 5

  def perform(phone_number, token, account_number, amount, project_id)
  	project_account = Project.find(project_id).account
    resource = RestClient::Resource.new "https://mobilnik.kg/transfer_request/?phonenumber=#{phone_number}&id=#{phone_number}&token=#{token}&client=4&service_id=1&customer_id=#{project_account}&amount=#{amount}&comment=perevod&status=2"
    response = resource.get
    parsed_response = ActiveSupport::JSON.decode(response.to_str)
    if parsed_response['status_code'] == 0
      begin
        MobilnikPayment.create!(project_id:project_id, phone_number:phone_number, account_number:account_number, amount:amount)
        Project.where(:id => project_id).update_all(["budget_donated = budget_donated + ?", amount])
      rescue Exception 
        return
      end 
    end
  end
end