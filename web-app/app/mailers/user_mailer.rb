class UserMailer < ApplicationMailer
  def send_mail(project, subscribed_mail)
    @project = project
    @email = subscribed_mail.email
    @email_from = "noreply@example.com"
    mail(to: [ @email ], from: @email_from, subject: 'Confirmation mail from startup.kg' )
  end
  def send_report_mail(project, subscribed_mail)
    @project = project
    @email = subscribed_mail.email
    @email_from = "noreply@example.com"
    mail(to: [ @email ], from: @email_from, subject: "Startup.kg report #{@project.name}" )
  end

  def send_deadline_mail(project, subscribed_mail)
    @project = project
    @email = subscribed_mail.email
    @email_from = "noreply@example.com"
    mail(to: [ @email ], from: @email_from, subject: "Startup.kg report #{@project.name}" )
  end

   def send_feedback(email, name, user_email, body)
    @email = email
    @name = name
    @user_email = user_email
    @body = body
    mail(to: [ @email ], subject:"from crowdfunding", body: [@name, "\n", @user_email, "\n", @body])
  end
end
