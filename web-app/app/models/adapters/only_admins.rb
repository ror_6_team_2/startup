class Adapters::OnlyAdmins < ActiveAdmin::AuthorizationAdapter
  def authorized?(action, subject = nil)
    user.superadmin?
  end
end
