class SubscribedMail < ApplicationRecord
  belongs_to :project

  validates :email, email: true
end
