class Project < ApplicationRecord
  self.send(:include, Kaminari::ConfigurationMethods)

  def self.page(num = nil)
    limit(default_per_page).offset(default_per_page * ((num = num.to_i - 1) < 0 ? 0 : num)).extending do
      include Kaminari::ActiveRecordRelationMethods
      include Kaminari::PageScopeMethods
    end
  end

  before_save :calculate_percent_of_budget_donated

  has_many :comments, as: :commentable, dependent: :destroy
  has_many :project_members, dependent: :destroy
  has_many :subscribed_mails, dependent: :destroy
  accepts_nested_attributes_for :project_members, :reject_if => lambda {|a| a[:name].blank?}, :allow_destroy => true
  belongs_to :category

  has_attached_file :image,
                    styles: {medium: '400x400>', thumb: '300x300>'},
                    default_url: '/images/:style/missing.png'
  validates_attachment_content_type :image,
                                    content_type: ['image/jpeg', 'image/gif', 'image/png']

  validates :image, presence: true
  validates :name, presence: true,
            uniqueness: true,
            length: {minimum: 1}
  validates :description, presence: true,
            length: {maximum: 5400}
  validates :budget, presence: true,
            numericality: {greater_than: 0}
  validates :budget_description, presence: true,
            length: {maximum: 3600}
  validates :budget_donated, presence: true,
            numericality: {greater_than_or_equal_to: 0}
  validates :stage, presence: true
  validates :account, presence: true,
            uniqueness: true
  validates :status, presence: true
  validates :deadline, presence: true
  validates :cancel_reason, length: {maximum: 1800}
  validates :author_comment, length: {maximum: 1800}
  validates :other_category, length: {maximum: 50}
  validates :category_id, presence: true
  validate :deadline_cannot_be_in_the_past
  validate :other_category_should_be_specified

  filterrific(
      default_filter_params: {with_status: 'Активный', sorted_by: 'donation_ratio_desc'},
      available_filters: [
          :sorted_by,
          :with_status,
          :with_category
      ]
  )

  def deadline_cannot_be_in_the_past
    if !deadline.blank? and deadline < Time.now and status == 'Активный'
      errors.add(:deadline, "can't be in the past")
    end
  end

  def other_category_should_be_specified
    if category_id == 3 and other_category.blank?
      errors.add(:other_category, "can't be blank")
    end
  end

# скоуп для выборки по статусу работает с gem 'filterrific'
  scope :with_status, lambda {|flag|
    where(status: [*flag])
  }
# скоуп для выборки по категориям работает с gem 'filterrific'
  scope :with_category, lambda {|category|
    where(category_id: [*category])}
# скоуп для сортировки всех проектор работает с gem 'filerrific'
  scope :sorted_by, lambda {|sort_option|
    # extract the sort direction from the param value.
    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
    case sort_option.to_s
      when /^donation_ratio_/
        # Simple sort on the created_at column.
        # Make sure to include the table name to avoid ambiguous column names.
        # Joining on other tables is quite common in Filterrific, and almost
        # every ActiveRecord table has a 'created_at' column.
        order("projects.donation_ratio #{ direction }")
      when /^deadline_/
        # Simple sort on the name colums
        order("projects.deadline #{ direction }")
      when /^created_at_/
        # This sorts by a student's country name, so we need to include
        # the country. We can't use JOIN since not all students might have
        # a country.
        order("projects.created_at #{ direction }")
      when /^rating_/
        # collect all comments and group by projtcts id and order by amaunt of comments
        Project.joins(:comments).group("projects.id").order("count(projects.id) #{ direction }")
        order("projects.click #{ direction }")
      else
        raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect }")
    end
  }

  def self.options_for_sorted_by
    [
        ['Дата создания (по убыванию)', 'created_at_desc'],
        ['Дата создания (по возрастанию', 'created_at_asc'],
        ['Сроку сбора (по возрастанию)', 'deadline_asc'],
        ['Сроку сбора (по убыванию)', 'deadline_desc'],
        ['По сбору денег (по возрастанию)', 'donation_ratio_asc'],
        ['По сбору денег (по убыванию)', 'donation_ratio_desc'],
        ['Популярность (по убыванию)', 'rating_desc'],
        ['Популярность (по возрастанию)', 'rating_asc']
    ]
  end

  private
  def calculate_percent_of_budget_donated
    self.donation_ratio = ((self.budget_donated.to_f*100)/self.budget.to_f).round()
  end

end
