class Category < ApplicationRecord
	has_many :projects, dependent: :destroy
	validates :name, presence: true,
  				   uniqueness: true,
  				   length: {minimum:1}
    validates :description, length: {maximum: 5400}
end
