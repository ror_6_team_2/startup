class Comment < ApplicationRecord
  belongs_to :commentable, polymorphic: true
  belongs_to :user
  has_ancestry
  validates :body, presence: true

  def edited
    self.updated_at > self.created_at
  end
  
end
