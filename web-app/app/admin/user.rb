ActiveAdmin.register User do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :email, :occupation, :work_experience, :country, :region, :age, :email, :password, :roles
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

form do |f|
  f.inputs do
    f.input :name
    f.input :email
    f.input :password
    f.input :occupation
    f.input :work_experience
    f.input :country
    f.input :region
    f.input :age
    f.input :role, :as => :select, :collection => User.roles
    f.actions
  end
end

index do
  selectable_column
  column :name
  column :email
  column :role
  actions
end

show do
  attributes_table do
    row :name
    row :email
    row :occupation
    row :work_experience
    row :country
    row :region
    row :age
    row :created_at
    row :updated_at
    row :role
  end
end

end
