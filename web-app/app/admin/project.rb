ActiveAdmin.register Project do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
controller do
  def new
  	redirect_to new_project_path
 	end

  def edit
    @project = Project.find(params[:id])
    redirect_to edit_project_path(@project)
  end
end

index do
  selectable_column
  column :name
  column :status
  column :created_at
  column :deadline
  column :category
  actions
end

show do
  attributes_table do
    row :name
    row :description
    row :budget
    row :budget_description
    row :budget_donated
    row :stage
    row :account
    row :status
    row :created_at
    row :updated_at
    row :deadline
    row :cancel_date
    row :cancel_reason
    row :author_comment
    row :category
  end
end

end
