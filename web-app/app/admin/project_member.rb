ActiveAdmin.register ProjectMember do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :project, :name, :role, :resume_file_name, :resume_content_type, :resume_file_size, :resume_updated_at
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
form do |f|
  f.inputs do
    f.input :name
    f.input :project
    f.input :role
    f.actions
  end
end

index do
  selectable_column
  column :user
  column :project
  column :role
  actions
end

actions :all, :except => [:new]

end
