ActiveAdmin.register Incubator do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
permit_params :name, :image

controller do
  def new
  	redirect_to new_incubator_path
 	end

  def edit
    @incubator = Incubator.find(params[:id])
    redirect_to edit_incubator_path(@incubator)
  end
end

#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

end
