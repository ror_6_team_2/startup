module ProjectsHelper
  def number_of_days_till_deadline(project)
    ((Time.at(project.deadline - Time.now)).utc).strftime("%m месяца %d дней %H часов %M минут")
  end

  def subscribed_email?(project)
    emails = project.subscribed_mails
    result = nil
    emails.each do |subscriber|
      if current_user.email == subscriber.email
        result = true
      else
        result = false
      end
    end
    return result
  end
  def email_unsubscribe
    SubscribedMail.all.each do |subscriber|
      if subscriber.email == current_user.email
        return subscriber.id
      end
    end
  end
  
  def nested_comments(comments, project)
    comments.map do |comment, sub_comments|
      render(:template => "comments/_comment", :locals => {:comment => comment, :commentable => project}) + content_tag(:div, nested_comments(sub_comments,project), class: ["nested_comments"])
    end.join.html_safe
  end
end
