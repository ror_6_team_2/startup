json.extract! incubator, :id, :name, :created_at, :updated_at
json.url incubator_url(incubator, format: :json)
