require 'test_helper'

class ProjectsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @project = projects(:one)
  end

  test "should get index" do
    get projects_url
    assert_response :success
  end

  test "should get new" do
    get new_project_url
    assert_response :success
  end

  test "should create project" do
    assert_difference('Project.count') do
      post projects_url, params: { project: { account: @project.account, author_comment: @project.author_comment, budget: @project.budget, budget_description: @project.budget_description, budget_donated: @project.budget_donated, cancel_date: @project.cancel_date, cancel_reason: @project.cancel_reason, category_id: @project.category_id, deadline: @project.deadline, description: @project.description, name: @project.name, other_category: @project.other_category, stage: @project.stage, status: @project.status } }
    end

    assert_redirected_to project_url(Project.last)
  end

  test "should show project" do
    get project_url(@project)
    assert_response :success
  end

  test "should get edit" do
    get edit_project_url(@project)
    assert_response :success
  end

  test "should update project" do
    patch project_url(@project), params: { project: { account: @project.account, author_comment: @project.author_comment, budget: @project.budget, budget_description: @project.budget_description, budget_donated: @project.budget_donated, cancel_date: @project.cancel_date, cancel_reason: @project.cancel_reason, category_id: @project.category_id, deadline: @project.deadline, description: @project.description, name: @project.name, other_category: @project.other_category, stage: @project.stage, status: @project.status } }
    assert_redirected_to project_url(@project)
  end

  test "should destroy project" do
    assert_difference('Project.count', -1) do
      delete project_url(@project)
    end

    assert_redirected_to projects_url
  end
end
