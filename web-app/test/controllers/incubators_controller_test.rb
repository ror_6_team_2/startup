require 'test_helper'

class IncubatorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @incubator = incubators(:one)
  end

  test "should get index" do
    get incubators_url
    assert_response :success
  end

  test "should get new" do
    get new_incubator_url
    assert_response :success
  end

  test "should create incubator" do
    assert_difference('Incubator.count') do
      post incubators_url, params: { incubator: { name: @incubator.name } }
    end

    assert_redirected_to incubator_url(Incubator.last)
  end

  test "should show incubator" do
    get incubator_url(@incubator)
    assert_response :success
  end

  test "should get edit" do
    get edit_incubator_url(@incubator)
    assert_response :success
  end

  test "should update incubator" do
    patch incubator_url(@incubator), params: { incubator: { name: @incubator.name } }
    assert_redirected_to incubator_url(@incubator)
  end

  test "should destroy incubator" do
    assert_difference('Incubator.count', -1) do
      delete incubator_url(@incubator)
    end

    assert_redirected_to incubators_url
  end
end
