class AddAttachmentImageToIncubators < ActiveRecord::Migration[5.1]
  def self.up
    change_table :incubators do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :incubators, :image
  end
end
