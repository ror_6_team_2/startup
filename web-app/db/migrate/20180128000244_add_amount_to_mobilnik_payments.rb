class AddAmountToMobilnikPayments < ActiveRecord::Migration[5.1]
  def change
    add_column :mobilnik_payments, :amount, :integer
  end
end
