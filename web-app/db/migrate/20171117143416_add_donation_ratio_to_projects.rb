class AddDonationRatioToProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :donation_ratio, :float, :default => 0
  end
end
