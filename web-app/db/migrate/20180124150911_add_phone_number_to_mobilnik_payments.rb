class AddPhoneNumberToMobilnikPayments < ActiveRecord::Migration[5.1]
  def change
    add_column :mobilnik_payments, :phone_number, :string
  end
end
