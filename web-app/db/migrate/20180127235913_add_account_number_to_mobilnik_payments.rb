class AddAccountNumberToMobilnikPayments < ActiveRecord::Migration[5.1]
  def change
    add_column :mobilnik_payments, :account_number, :string
  end
end
