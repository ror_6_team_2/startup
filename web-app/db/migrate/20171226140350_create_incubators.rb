class CreateIncubators < ActiveRecord::Migration[5.1]
  def change
    create_table :incubators do |t|
      t.string :name

      t.timestamps
    end
  end
end
