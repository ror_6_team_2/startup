class CreateSubscribedMails < ActiveRecord::Migration[5.1]
  def change
    create_table :subscribed_mails do |t|
      t.string :name, default: 'Subscriber'
      t.string :email
      t.references :project, foreign_key: true

      t.timestamps
    end
  end
end
