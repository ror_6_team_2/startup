class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :occupation
      t.text :work_experience
      t.string :country
      t.string :region
      t.integer :age

      t.timestamps
    end
  end
end
