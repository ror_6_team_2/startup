class CreateProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :projects do |t|
      t.string :name
      t.text :description
      t.integer :budget
      t.text :budget_description
      t.integer :budget_donated
      t.string :stage
      t.integer :account
      t.string :status
      t.datetime :deadline
      t.datetime :cancel_date
      t.text :cancel_reason
      t.text :author_comment
      t.text :other_category
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
