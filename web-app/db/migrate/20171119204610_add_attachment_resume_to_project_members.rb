class AddAttachmentResumeToProjectMembers < ActiveRecord::Migration[4.2]
  def self.up
    change_table :project_members do |t|
      t.attachment :resume
    end
  end

  def self.down
    remove_attachment :project_members, :resume
  end
end
