class RemoveUserIdFromProjectMembers < ActiveRecord::Migration[5.1]
  def change
    remove_column :project_members, :user_id, :bigint
  end
end
