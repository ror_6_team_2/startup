class RemoveTokenFromMobilnikPayments < ActiveRecord::Migration[5.1]
  def change
    remove_column :mobilnik_payments, :token, :string
  end
end
