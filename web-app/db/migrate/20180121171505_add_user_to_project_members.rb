class AddUserToProjectMembers < ActiveRecord::Migration[5.1]
  def change
    add_reference :project_members, :user, foreign_key: true
  end
end
