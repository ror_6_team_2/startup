class AddClickToProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :click, :integer, default: 1
  end
end
