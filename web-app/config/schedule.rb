#
set :output, "#{path}/log/cron.log"
env :PATH, ENV['PATH']
ENV.each{|k,v| env(k, v)}

every :day, :at => '12am' do
  rake 'check_expiration_date'
end

every :friday, :at => '12pm' do
  rake 'send_mail'
end