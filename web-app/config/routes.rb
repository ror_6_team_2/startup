Rails.application.routes.draw do
  require 'sidekiq/web'

  get '/feedbacks/new'
  post '/feedbacks/create'
  get 'user_projects/index'

  resources :incubators
  post '/tinymce_assets' => 'tinymce_assets#create'

  devise_for :users
  ActiveAdmin.routes(self)

  resources :projects do
    resources :comments, module: :projects
    resources :project_members
    resources :mobilnik_payments do
      collection do
        get 'user_info_request'
        get 'admin_phone_number_request'
        get 'mobilnik_code_request'
        get 'mobilnik_token_request'
        get 'mobilnik_code_request_for_admin'
        get 'mobilnik_token_request_for_admin'
      end
    end
  end
  
  resources :categories
  resources :subscribed_mails
  resources :incubators
  
  authenticate :user, lambda { |u| u.superadmin? } do
    mount Sidekiq::Web => '/sidekiq'
  end

  root 'projects#index'
end
